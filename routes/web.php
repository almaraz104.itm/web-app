<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['reset'=>false]);
Route::post('registro/store','RegistroController@store')->name('registro.store');

Route::group(['middleware' => 'auth'], function () {
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/egresados', 'EgresadosController@index')->name('egresados.index');
Route::get('/egresados/create', 'EgresadosController@create')->name('egresados.create');
Route::post('egresados/store','EgresadosController@store')->name('egresados.store');
Route::delete('egresados/{id}','EgresadosController@destroy')->name('egresados.destroy');
Route::get('egresados/{id}/edit','EgresadosController@edit')->name('egresados.edit');
Route::patch('egresados/{id}','EgresadosController@update')->name('egresados.update');


Route::get('/programa', 'ProgramaController@index')->name('programa.index');
Route::post('programa/store','ProgramaController@store')->name('programa.store');
Route::get('/templatePrograma','ProgramaController@show')->name('templatePrograma.show');

Route::get('/estadistica','chart@index')->name('chart.index');
});