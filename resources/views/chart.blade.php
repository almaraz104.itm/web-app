<!-- @extends('layouts.app') -->
@section('content')

<?php 

  $con=new mysqli("localhost", "root","","amaro");
  $sql="select nombre, cantidad from carreras";
  $res=$con->query($sql);

?>



<!-- Sidebar -->
  <div class="d-flex" id="wrapper">

    <div class="bg-light border-right" id="sidebar-wrapper" >
      <div class="sidebar-heading" style="background-color: #04C496">
        <img class="" src={{asset('images/Icon/itm.png')}} width="100"  />  
       <label class="text-white">IT Morelia</label>
      </div>
      <div class="list-group list-group-flush">
        
        <a href="{{route('egresados.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/pagos.png')}}  />
          Pagos realizados
        </a>
      <a href="{{route('programa.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/programa.png')}}  />
          Generar programa
        </a>
        <a href="{{route('chart.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/grafica.png')}}  />
          Estadísticas
        </a>
        
      </div>

  </div>
<!-- /#sidebar-wrapper -->



<div class="container">

  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
         <?php 
          while ($fila=$res->fetch_assoc()) {
            echo "['".$fila["nombre"]."',".$fila["cantidad"]."],";
          }
          
          ?>
        ]);

        // Set chart options
        var options = {'title':'Porcentaje de alumnos graduados por carrera',
                       'width':800,
                       'height':600};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
  </head>

  <body>
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
  </body>


</div>



@endsection