@extends('layouts.app')
@section('content')

<!-- Sidebar -->
<div class="d-flex" id="wrapper">

    <div class="bg-light border-right" id="sidebar-wrapper" >
      <div class="sidebar-heading" style="background-color: #04C496">
        <img class="" src={{asset('images/Icon/itm.png')}} width="100"  />  
       <label class="text-white">IT Morelia</label>
      </div>
      <div class="list-group list-group-flush">
        <a href="{{route('egresados.index')}}" class="list-group-item list-group-item-action bg-light">Pagos realizados</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Generar programa</a>
        <a href="#" class="list-group-item list-group-item-action bg-light">Estadísticas</a>
      </div>
      
  </div>
<!-- /#sidebar-wrapper -->

<div class="container">

@foreach($datos as $item)
<br/>
<form method="POST" action="{{route('egresados.update',$item->id)}}" style="margin-left:1%">
   @csrf
   {{method_field('PATCH')}}
                        <div class="form-group">
                        <label for="name" class="control-label">{{ 'Nombre' }}</label>
                        <input type="text" class="form-control" name="nombre" value="{{ $item->nombre }}">
                        </div>

                        <div class="form-group">
                        <label for="apaterno" class="control-label">{{ 'Apellido paterno' }}</label>
                        <input type="text" class="form-control" name="apaterno" value="{{ $item->apaterno }}">
                        </div>

                        <div class="form-group">
                        <label for="amaterno" class="control-label">{{ 'Apellido materno' }}</label>
                        <input type="text" class="form-control" name="amaterno" value="{{ $item->amaterno }}">
                        </div>

                        <div class="form-group">
                        <label for="telefono" class="control-label">{{ 'Teléfono' }}</label>
                        <input type="text" class="form-control" name="telefono" value="{{ $item->telefono }}">
                        </div>

                        <div class="form-group">
                        <label for="ncontrol" class="control-label">{{ 'Número de control' }}</label>
                        <input type="text" class="form-control" name="ncontrol" value="{{ $item->ncontrol }}">
                        </div>

                        <div class="form-group">
                        <label for="carrera" class="control-label">{{ 'Carrera' }}</label>
                        <input type="carrera" class="form-control" name="carrera" value="{{ $item->carrera }}">
                        </div>

                        <div class="form-group">
                        <label for="foto" class="control-label">{{ 'Comprobante de pago' }}</label>
                        <img src=" {{ asset('storage').'/'. $item->foto}}" alt="comprobante" width="150">
                        <input type="file"  class="form-control" name="foto" value="">
                        </div>
                        <br/>

                      <div class="col text-center" style="margin-bottom:5%">
                          <a href="{{route('egresados.index')}}" class="btn btn-danger btn-lg" >
                            Cancelar
                          </a>
                        <input type="submit" class=" btn text-white btn-lg" style=" background-color: #04C496; margin-left:20%" value="Aceptar" >
                        
                      </div>

                    </form>


</div>
@endforeach
@endsection