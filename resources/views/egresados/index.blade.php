@extends('layouts.app')
@section('content')



<!-- Sidebar -->
  <div class="d-flex" id="wrapper">

    <div class="bg-light border-right" id="sidebar-wrapper" >
      <div class="sidebar-heading" style="background-color: #04C496">
        <img class="" src={{asset('images/Icon/itm.png')}} width="100"  />  
       <label class="text-white">IT Morelia</label>
      </div>
      <div class="list-group list-group-flush">
        
        <a href="{{route('egresados.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/pagos.png')}}  />
          Pagos realizados
        </a>
      <a href="{{route('programa.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/programa.png')}}  />
          Generar programa
        </a>
        <a href="{{route('chart.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/grafica.png')}}  />
          Estadísticas
        </a>
        
      </div>

  </div>
<!-- /#sidebar-wrapper -->

<div class="container">


      <!-- Muestro mensaje de exito-->
      @if(Session::has('Mensaje'))
      <div class="alert alert-success" role="alert" style="margin-left:-1.3%">
          {{Session::get('Mensaje')}}
      </div>
    @endif
    <!-- Fin mensaje-->

<table class="table table-light table-hover table-responsive-sm" style="margin-left:-1.3%">
   
    <thead style="background-color: #2f3848" class="text-white">
        <tr>   
            <th>ID</th>
            <th>Nombre</th>
            <th>Teléfono</th>
            <th>No Control</th>
            <th>Carrera</th>
            <th>Comprobante</th>
             <th>Acciones</th>
        </tr>
    </thead>
    <tbody>

    @foreach ($pagos as $item )

        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$item->nombre}} {{$item->apaterno}} {{$item->amaterno}}</td>
            <td>{{$item->telefono}}</td>
            <td>{{$item->ncontrol}}</td>
            <td>{{$item->carrera}}</td>
            <td>
            
            <img src=" {{ asset('storage').'/'. $item->foto}}" class="img-thumbnail img-fluid" alt="" width="100" onClick="this.style.height = '500px'; this.style.width = '200px';">
    
            </td>
            <td>
                <form method="post" action="{{route('egresados.destroy',$item->id)}}" style="display: inline;">
                @csrf
                {{method_field('DELETE')}}
                <button class="btn btn-danger" type="submit" onclick="return confirm('¿Desea borrar este registro?');" style="margin-right:5%"> 
                  <img class="" src={{asset('images/Icon/eliminar.png')}}  />  
                </button>
                </form>

                <a href="{{route('egresados.edit',$item->id)}}" class="btn btn-warning" style="margin-right:5%">
                  <img class="" src={{asset('images/Icon/editar.png')}}  /> 
                </a>
                <a href="" class="btn btn-success">
                  <img class="" src={{asset('images/Icon/comprobado.png')}}  /> 
                </a>
              
            </td>
        </tr>
        
    @endforeach
       
    </tbody>
</table>
    {{--Paginación--}}
    <div class="row justify-content-center">                       
      {{$pagos->links()}}                  
    </div>
    
</div>
@endsection