@extends('layouts.app')
@section('content')

<!-- Sidebar -->
<div class="d-flex" id="wrapper">

    <div class="bg-light border-right" id="sidebar-wrapper" >
      <div class="sidebar-heading" style="background-color: #04C496">
        <img class="" src={{asset('images/Icon/itm.png')}} width="100"  />  
       <label class="text-white">IT Morelia</label>
      </div>
      <div class="list-group list-group-flush">
        
        <a href="{{route('egresados.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/pagos.png')}}  />
          Pagos realizados
        </a>
        <a href="{{route('programa.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/programa.png')}}  />
          Generar programa
        </a>
        <a href="{{route('templatePrograma.show')}}" class="list-group-item list-group-item-action bg-light">
            <img class="" src={{asset('images/Icon/ver.png')}}  />
            Ver Programa 
        </a>
        <a href="{{route('chart.index')}}" class="list-group-item list-group-item-action bg-light">
          <img class="" src={{asset('images/Icon/grafica.png')}}  />
          Estadísticas
        </a>
        
      </div>

  </div>
<!-- /#sidebar-wrapper -->



<div class="container">
    
      <!-- Muestro mensaje de exito-->
      @if(Session::has('Mensaje'))
      <div class="alert alert-success" role="alert" style="margin-left:-1.3%">
          {{Session::get('Mensaje')}}
      </div>
    @endif
    <!-- Fin mensaje-->

    <br/>
    
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="card shadow p-2">
                <div class="card-header text-white" style="background-color: #2f3848"><h3>{{ __('Programar Ceremonia') }}</h3></div>

                <div class="card-body">
                    <form method="POST" action="{{route('programa.store')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="lugar" class="col-md-4 col-form-label text-md-right">{{ __('Lugar:') }}</label>

                            <div class="col-md-6">
                                <input id="lugar" type="text" class="form-control @error('lugar') is-invalid @enderror" name="lugar" value="{{ old('lugar') }}" required autocomplete="lugar" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="direccion" class="col-md-4 col-form-label text-md-right">{{ __('Dirección:') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="horatempra" class="col-md-4 col-form-label text-md-right">{{ __('Hora: (Mañana)') }}</label>

                            <div class="col-md-6">
                                <input id="horatempra" type="time" class="form-control @error('horatempra') is-invalid @enderror" name="horatempra"   required autocomplete="horatempra" step="1" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="horatarde" class="col-md-4 col-form-label text-md-right">{{ __('Hora: (Tarde)') }}</label>

                            <div class="col-md-6">
                                <input id="horatarde" type="time" class="form-control @error('horatarde') is-invalid @enderror" name="horatarde"   required autocomplete="horatarde" step="1" autofocus>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="fecha" class="col-md-4 col-form-label text-md-right">{{ __('Fecha:') }}</label>

                            <div class="col-md-6">
                                <input id="fecha" type="date" class="form-control @error('fecha') is-invalid @enderror" name="fecha"   required autocomplete="fecha" autofocus>
                            </div>
                        </div>

                        <br/>

                        <div class="container" style="margin-left:8%">

                            <div class="row justify-content-center">
                                

                            <a href="{{route('egresados.index')}}" class="btn btn-lg btn-danger text-white " style="margin-right:10%">
                                    {{ __('Cancelar') }}
                            </a>

                                <button type="submit" class="btn btn-lg text-white " style="background-color: #04C496">
                                    {{ __('Programar') }}
                                </button>

                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>













@endsection