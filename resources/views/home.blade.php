@extends('layouts.app')
@section('content')

  <!-- Muestro mensaje de exito-->
        @if(Session::has('Mensaje'))
           <div class="alert alert-success" role="alert">
               {{Session::get('Mensaje')}}
           </div>
        @endif
  <!-- Fin mensaje-->

<br/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white" style="background-color: #2f3848"><h3>Ingresa tus datos</h3></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
        
                    <form action="{{route('egresados.store')}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                        @csrf

                        <div class="form-group">
                        <label for="name" class="control-label">{{ 'Nombre' }}</label>
                        <input type="text" class="form-control" name="nombre" value="">
                        </div>

                        <div class="form-group">
                        <label for="apaterno" class="control-label">{{ 'Apellido paterno' }}</label>
                        <input type="text" class="form-control" name="apaterno" value="">
                        </div>

                        <div class="form-group">
                        <label for="amaterno" class="control-label">{{ 'Apellido materno' }}</label>
                        <input type="text" class="form-control" name="amaterno" value="">
                        </div>

                        <div class="form-group">
                        <label for="telefono" class="control-label">{{ 'Teléfono' }}</label>
                        <input type="text" class="form-control" name="telefono" value="">
                        </div>

                        <div class="form-group">
                        <label for="ncontrol" class="control-label">{{ 'Número de control' }}</label>
                        <input type="text" class="form-control" name="ncontrol" value="">
                        </div>

                        <div class="form-group">
                        <label for="carrera" class="control-label">{{ 'Carrera' }}</label>
                        
                        <select class="form-control" name="carrera" required>
                            <option>{{'---- Seleccione la carrera ----'}}</option>
                              
                        @foreach($carreras as $item) 
                        <option>{{$item->nombre}}</option> 
                        @endforeach  

                        </select>
                    
                        </div>

                        <div class="form-group">
                        <label for="foto" class="control-label">{{ 'Comprobante de pago' }}</label>
                        <input type="file"  class="form-control" name="foto" value="">
                        </div>
                        <br/>

                        <div class="col text-center">
                        <input type="submit" class=" btn text-white btn-lg" style=" background-color: #04C496;" value="Aceptar">
                        </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
