<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Programa extends Model
{
    //
    protected $table = 'programa';
    protected $fillable = [
        'lugar',
        'direccion',
        'horatempra',
        'horatarde',
        'fecha',
    ];
}
