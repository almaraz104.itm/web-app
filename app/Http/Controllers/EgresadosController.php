<?php

namespace App\Http\Controllers;

use App\Egresados;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class EgresadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pagos = Egresados::paginate(5);
        return view('egresados.index', compact('pagos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('egresados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $datosEgresado=request()->except('_token','_method');

        if($request->hasFile('foto')){

            $datosEgresado['foto']=$request->file('foto')->store('uploads','public');

        }

        Egresados::insert($datosEgresado);

        return redirect('/home')->with('Mensaje','¡Datos registrados con éxito! Pronto se enviará tu invitación ');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Egresados  $egresados
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Egresados  $egresados
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $datos = DB::table('egresados')
                    ->where('id', '=', $id)
                    ->get();
        return view('egresados.edit',compact('datos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Egresados  $egresados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $datosEgresado=request()->except('_token','_method');

        if($request->hasFile('foto')){
            $egresado=Egresados::findOrFail($id);
            Storage::delete('public/'.$egresado->foto);
            $datosEgresado['foto']=$request->file('foto')->store('uploads','public');

        }

    //    return response()->json($datosEgresado);

        egresados::where('id','=',$id)->update($datosEgresado);
       
        return redirect('egresados')->with('Mensaje','¡Datos modificados con exito!');

      //   $datos=egresados::findOrFail($id);
        // Storage::delete('public/'.$egresado->foto);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Egresados  $egresados
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $egresado=Egresados::findOrFail($id);

        if(Storage::delete('public/'.$egresado->foto)){
             Egresados::destroy($id);
        }
       
        return redirect('egresados')->with('Mensaje','¡Registro borrado con exito!');
        
    }
}
