<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carreras;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $carreras = Carreras::all();
        return view('home', compact('carreras'));
        //return view('home');
    }
}
